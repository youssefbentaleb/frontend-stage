import React from 'react';
import { Route } from 'react-router-dom';
import history from '../../history';


const LayoutRoute = ({ component: Component, layout: Layout, ...rest }) => (
  
  <Route history={history}
    {...rest}
    render={props => (
      
      <Layout>
        <Component {...props} /> 
      
      </Layout>
    )}
  />
);

export default LayoutRoute;

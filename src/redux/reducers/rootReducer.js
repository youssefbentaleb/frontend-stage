import {combineReducers} from 'redux';  
import  { reducer as formReducer }  from 'redux-form';




const rootReducer = combineReducers({  
  // short hand property names
  form: formReducer
})

export default rootReducer;  
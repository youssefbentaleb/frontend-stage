## Quick Start

1.  Clone the repo `git clone https://gitlab.com/youssefbentaleb/frontend-stage.git`
2.  Go to your project folder from your terminal
3.  Run: `npm install`
4.  After install, run: `npm run start`
5.  It will open your browser(http://localhost:3000)
